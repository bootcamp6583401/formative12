package formative.twelve.task1.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.twelve.task1.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}