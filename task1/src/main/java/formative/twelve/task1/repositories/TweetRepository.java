package formative.twelve.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.twelve.task1.models.Tweet;

public interface TweetRepository extends CrudRepository<Tweet, Integer> {
    List<Tweet> findByUserId(int user_id);

}