package formative.twelve.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.twelve.task1.models.Tweet;
import formative.twelve.task1.models.User;
import formative.twelve.task1.repositories.TweetRepository;
import formative.twelve.task1.repositories.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;

@Controller
@RequestMapping(path = "/tweets")
public class TweetController {
    @Autowired
    private TweetRepository tweetRepo;

    @Autowired
    private UserRepository userRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewTweet(@RequestBody Tweet blog) {

        Tweet n = new Tweet();
        n.setContent(blog.getContent());
        n.setCreatedAt(LocalDateTime.now());
        tweetRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Tweet> getAllTweets() {
        return tweetRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Tweet> deleteTweet(@PathVariable int id) {
        Optional<Tweet> optionalTweet = tweetRepo.findById(id);
        if (optionalTweet.isPresent()) {
            Tweet foundTweet = optionalTweet.get();
            tweetRepo.delete(foundTweet);
            return new ResponseEntity<>(foundTweet, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Tweet> getTweetById(@PathVariable("id") int id) {
        Optional<Tweet> userData = tweetRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Tweet> updateTweet(@PathVariable("id") int id, @RequestBody Tweet blog) {
        Optional<Tweet> tutorialData = tweetRepo.findById(id);

        if (tutorialData.isPresent()) {
            Tweet _blog = tutorialData.get();
            _blog.setContent(blog.getContent());
            return new ResponseEntity<>(tweetRepo.save(_blog), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/author/{authorId}")
    public ResponseEntity<Tweet> setAuthor(@PathVariable("id") int id, @PathVariable("authorId") int authorId) {
        Optional<Tweet> tutorialData = tweetRepo.findById(id);
        Optional<User> authorData = userRepo.findById(authorId);

        if (tutorialData.isPresent() && authorData.isPresent()) {
            Tweet _blog = tutorialData.get();
            User _author = authorData.get();

            _blog.setUser(_author);

            return new ResponseEntity<>(tweetRepo.save(_blog), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}