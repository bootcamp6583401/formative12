CREATE TABLE brand (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    artnumber VARCHAR(50),
    name VARCHAR(50) NOT NULL,
    description VARCHAR(150),
    brand_id INT,
    FOREIGN KEY (brand_id) REFERENCES brand(id)
);

INSERT INTO brand (name) VALUES ('BrandA');
INSERT INTO brand (name) VALUES ('BrandB');

INSERT INTO product (artnumber, name, description, brand_id) VALUES ('123456', 'ProductA1', 'Description for ProductA1', 1);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('789012', 'ProductA2', 'Description for ProductA2', 1);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('345678', 'ProductA3', 'Description for ProductA3', 1);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('901234', 'ProductA4', 'Description for ProductA4', 1);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('567890', 'ProductA5', 'Description for ProductA5', 1);

INSERT INTO product (artnumber, name, description, brand_id) VALUES ('111222', 'ProductB1', 'Description for ProductB1', 2);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('333444', 'ProductB2', 'Description for ProductB2', 2);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('555666', 'ProductB3', 'Description for ProductB3', 2);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('777888', 'ProductB4', 'Description for ProductB4', 2);
INSERT INTO product (artnumber, name, description, brand_id) VALUES ('999000', 'ProductB5', 'Description for ProductB5', 2);
