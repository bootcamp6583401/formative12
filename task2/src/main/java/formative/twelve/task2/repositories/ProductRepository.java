package formative.twelve.task2.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.twelve.task2.models.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findByBrandId(int brandId);

}