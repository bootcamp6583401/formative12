package formative.twelve.task2.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.twelve.task2.models.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer> {

}