package formative.twelve.task2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.twelve.task2.models.Product;
import formative.twelve.task2.models.Brand;
import formative.twelve.task2.repositories.ProductRepository;
import formative.twelve.task2.repositories.BrandRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/products")
public class ProductController {
    @Autowired
    private ProductRepository prodRepo;

    @Autowired
    private BrandRepository userRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewProduct(@RequestBody Product product) {

        Product n = new Product();
        n.setName(product.getName());
        n.setArtnumber(product.getArtnumber());
        n.setDescription(product.getDescription());
        prodRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Product> getAllProducts() {
        return prodRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable int id) {
        Optional<Product> optionalProduct = prodRepo.findById(id);
        if (optionalProduct.isPresent()) {
            Product foundProduct = optionalProduct.get();
            prodRepo.delete(foundProduct);
            return new ResponseEntity<>(foundProduct, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") int id) {
        Optional<Product> userData = prodRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        Optional<Product> tutorialData = prodRepo.findById(id);

        if (tutorialData.isPresent()) {
            Product _product = tutorialData.get();
            _product.setName(product.getName());
            _product.setArtnumber(product.getArtnumber());
            _product.setDescription(product.getDescription());
            return new ResponseEntity<>(prodRepo.save(_product), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/brand/{brandId}")
    public ResponseEntity<Product> setAuthor(@PathVariable("id") int id, @PathVariable("brandId") int brandId) {
        Optional<Product> tutorialData = prodRepo.findById(id);
        Optional<Brand> brandData = userRepo.findById(brandId);

        if (tutorialData.isPresent() && brandData.isPresent()) {
            Product _product = tutorialData.get();
            Brand _brand = brandData.get();

            _product.setBrand(_brand);

            return new ResponseEntity<>(prodRepo.save(_product), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}