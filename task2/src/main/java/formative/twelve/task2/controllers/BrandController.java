package formative.twelve.task2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.twelve.task2.models.Product;
import formative.twelve.task2.models.Brand;
import formative.twelve.task2.repositories.ProductRepository;
import formative.twelve.task2.repositories.BrandRepository;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/brands")
public class BrandController {
    @Autowired
    private BrandRepository brandRepo;

    @Autowired
    private ProductRepository prodRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewBrand(@RequestParam String name, @RequestParam String email) {

        Brand n = new Brand();
        n.setName(name);
        brandRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Brand> getAllBrands() {
        return brandRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Brand> deleteBrand(@PathVariable int id) {
        Optional<Brand> optionalBrand = brandRepo.findById(id);
        if (optionalBrand.isPresent()) {
            Brand foundBrand = optionalBrand.get();
            brandRepo.delete(foundBrand);
            return new ResponseEntity<>(foundBrand, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Brand> getBrandById(@PathVariable("id") int id) {
        Optional<Brand> brandData = brandRepo.findById(id);
        List<Product> products = prodRepo.findByBrandId(id);

        if (brandData.isPresent()) {
            Brand brand = brandData.get();
            brand.setProductList(products);
            return new ResponseEntity<>(brand, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/products")
    public ResponseEntity<List<Product>> getBrandBlogs(@PathVariable("id") int id) {
        Optional<Brand> brandData = brandRepo.findById(id);
        List<Product> products = prodRepo.findByBrandId(id);

        if (brandData.isPresent()) {
            return new ResponseEntity<>(products, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Brand> updateBrand(@PathVariable("id") int id, @RequestBody Brand brand) {
        Optional<Brand> brandData = brandRepo.findById(id);

        if (brandData.isPresent()) {
            Brand _brand = brandData.get();
            _brand.setName(brand.getName());
            return new ResponseEntity<>(brandRepo.save(_brand), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}